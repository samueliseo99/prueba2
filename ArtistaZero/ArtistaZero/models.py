from django.db import models


class Friedrich(models.Model):
    id=models.AutoField(primary_key=True)
    nombre=models.CharField(max_length=30)
    precio=models.IntegerField(default=100000)
    
    def __str__(self):
        return self.nombre
    
    
    
class Goya(models.Model):
    id=models.AutoField(primary_key=True)
    nombre=models.CharField(max_length=30)
    precio=models.IntegerField(default=85000)
    
    def __str__(self):
        return self.nombre
    
    
    
class Janmillet(models.Model):
    id=models.AutoField(primary_key=True)
    nombre=models.CharField(max_length=30)
    precio=models.IntegerField(default=90000)
    
    def __str__(self):
        return self.nombre
    
    
    
class Monet(models.Model):
    id=models.AutoField(primary_key=True)
    nombre=models.CharField(max_length=30)
    precio=models.IntegerField(default=130000)
    
    def __str__(self):
        return self.nombre
    
    
    
class Vangogh(models.Model):
    id=models.AutoField(primary_key=True)
    nombre=models.CharField(max_length=30)
    precio=models.IntegerField(default=200000)
    
    def __str__(self):
        return self.nombre
    
    
class Formulario(models.Model):
    nombre = models.CharField(max_length=50)
    apellido = models.CharField(max_length=50)
    telefono = models.IntegerField()
    correo = models.EmailField(max_length=50)
    mensaje = models.TextField(max_length=300)
    
    def __str__(self):
        return self.nombre
    
   
    

from django.contrib import admin

from .models import Friedrich, Goya, Janmillet, Monet, Vangogh, Formulario

admin.site.register(Friedrich)
admin.site.register(Goya)
admin.site.register(Janmillet)
admin.site.register(Monet)
admin.site.register(Vangogh)
admin.site.register(Formulario)


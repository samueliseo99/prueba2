"""ArtistaZero URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from . import view
from django.contrib.auth.views import LoginView, LogoutView


urlpatterns = [
    path('admin/', admin.site.urls),
    path('index/',view.index, name="Index"),
    path('friedrich/',view.friedrich, name="Friedrich"),
    path('goya/',view.goya, name="Goya"),
    path('jeanmillet/',view.jeanmillet, name="Jeanmillet"),
    path('monet/',view.monet, name="Monet"),
    path('vangogh/',view.vangogh, name="Vangogh"),
    path('formulario/',view.formulario, name="Formulario"),    
    path('devolucion/',view.devolucion, name="Devolucion"),   
    path('envio/',view.envio, name="Envio"), 
    path('pie/',view.pie, name="Pie"),
    path('politicas/',view.politicas, name="Politicas"),
    path('productos/',view.products, name="Productos"),
    path('home/',view.home, name="Home"),
    path('base/',view.base, name="Base"),
    path('accounts/', include("django.contrib.auth.urls")),
    path('login/',LoginView.as_view(template_name="login.html"), name="Login"),
    path('logout/',LogoutView.as_view(template_name="logout.html"), name="logout"),
    path('register/',view.register, name="Register"),
    
    
    
]

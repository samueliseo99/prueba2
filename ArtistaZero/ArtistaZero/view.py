from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.forms import AuthenticationForm
from .forms import CustomUserCreationForm 
from .forms import FormularioConsulta
from django.contrib.auth.forms import UserChangeForm
from django.contrib import messages
from django.core.mail import send_mail 
from django.core.mail import EmailMessage
from django.template.loader import render_to_string
from django.conf import settings






def index(request):
    return render(request,"index.html")

def friedrich(request):
    return render(request,"C.friedrich.html")


def goya(request):
    return render(request,"goya.html")

def jeanmillet(request):
    return render(request,"jeanmillet.html")

def monet(request):
    return render(request,"monet.html")

def vangogh(request):
    return render(request,"vangogh.html")

def devolucion(request):
    return render(request,"devolucion.html")

def envio(request):
    return render(request,"envio.html")

def pie(request):
    return render(request,"pie.html")

def politicas(request):
    return render(request,"politicas.html")

def home(request):
    return render(request,"home.html")

@login_required
def products(request):
    return render(request,"productos.html")

def base(request):
    return render(request,"base.html")

def exit(request):
    logout(request)
    return redirect('Index')

def register(request):
    if request.method == 'POST':
        form = CustomUserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data['username']
            messages.success(request, f'Usuario {username} creado')
            return redirect('Index')
    else:
        form = CustomUserCreationForm()
    
    context = { 'form' : form}
    return render(request, 'register.html', context)


def login(request):
    if request.method == 'POST':
        form = AuthenticationForm(request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('home')  # Redirige a la página de inicio después de iniciar sesión
            else:
                error = 'Usuario o contraseña incorrectos.'
        else:
            error = 'Formulario inválido.'
    else:
        form = AuthenticationForm()
        error = None

    context = {
        'form': form,
        'error': error,
    }

    return render(request, 'login.html', context)



def formulario(request):
    if request.method == 'POST':
        form = FormularioConsulta(request.POST)
        if form.is_valid():
            formulario = form.save(commit=False)
            formulario.save()
            subject = form.cleaned_data['nombre']
            message = form.cleaned_data['mensaje'] + ' correo: ' + form.cleaned_data['correo']
            email_from = settings.EMAIL_HOST_USER
            recipient_list = ['jfelipe.almendra.93@gmail.com']
            send_mail(subject, message, email_from, recipient_list)
            
            return redirect('Index')      
    else:
        form = FormularioConsulta() 
        
    return render(request, "FormularioConsulta.html", {'form': form})
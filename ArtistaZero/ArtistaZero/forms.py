from django import forms 
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from .models import Formulario

class CustomUserCreationForm(UserCreationForm):
    email = forms.EmailField()
    password1 = forms.CharField(label='Contraseña', widget=forms.PasswordInput)
    password2 = forms.CharField(label='Confirma Contraseña', widget=forms.PasswordInput)
   
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email', 'password1', 'password2']
        help_texts = {k:"" for k in fields}
        
        
        
class FormularioConsulta(forms.ModelForm):
    
    class Meta: 
        model = Formulario
        
        fields = [
            'nombre',
            'apellido',
            'telefono',
            'correo',
            'mensaje',
            
        ]
        labels = {
            'nombre': 'Nombre',
            'apellido': 'Apellido',
            'telefono': 'Telefono',
            'correo': 'Correo',
            'mensaje': 'Mensaje',
            
        }
        widgets ={
            'nombre': forms.TextInput(attrs={'class':'form-control'}),
            'apellido': forms.TextInput(attrs={'class':'form-control'}),
            'telefono': forms.TextInput(attrs={'class':'form-control'}),
            'correo': forms.TextInput(attrs={'class':'form-control'}),
            'mensaje': forms.TextInput(attrs={'class':'form-control'}),
        }
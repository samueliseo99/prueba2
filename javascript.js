function validarFormulario(evento) {
    evento.preventDefault(); // previene el comportamiento por defecto del formulario
  
    // Validación del campo nombre
    const nombreInput = document.getElementById("nombre");
    const nombre = nombreInput.value.trim(); // Eliminar espacios en blanco al inicio y al final
  
    if (nombre === "") {
      alert("El campo nombre es obligatorio.");
      nombreInput.focus();
      return false;
    }
  
    if (!/^[a-zA-Z\s]*$/.test(nombre)) {
      alert("El campo nombre solo puede contener letras y espacios.");
      nombreInput.focus();
      return false;
    }
  
    // Validación del campo correo electrónico
    const correoInput = document.getElementById("correo");
    const correo = correoInput.value.trim();
  
    if (correo === "") {
      alert("El campo correo electrónico es obligatorio.");
      correoInput.focus();
      return false;
    }
  
    if (!/\S+@\S+\.\S+/.test(correo)) {
      alert("El campo correo electrónico debe ser válido.");
      correoInput.focus();
      return false;
    }
  
    // Validación de los demás campos del formulario
  
    // Si se llega a este punto, el formulario es válido y se puede enviar
    return true;
  }


  // Obtener el botón de contacto
const btnContacto = document.querySelector('.boton-flotante');

// Escuchar el evento scroll en la ventana
window.addEventListener('scroll', () => {
  // Obtener la posición del pie de página
  const pieDePagina = document.querySelector('footer').getBoundingClientRect().top;

  // Obtener la posición actual de la ventana
  const posicionActual = window.innerHeight + window.pageYOffset;

  // Si la posición actual es mayor o igual a la posición del pie de página, mostrar el botón
  if (posicionActual >= pieDePagina) {
    btnContacto.style.display = 'block';
  } else {
    btnContacto.style.display = 'none';
  }
});



const form = document.getElementById('Formulario');
const confirmation = document.getElementById('confirmation');

form.addEventListener('submit', (event) => {
    event.preventDefault(); // prevenir el envío del formulario

    // verificar si el mensaje tiene al menos 10 caracteres
    const mensaje = document.getElementById('mensaje').value;
    if (mensaje.length < 10) {
        alert('El mensaje debe tener al menos 10 caracteres');
        return;
    }

    // mostrar el mensaje de confirmación
    confirmation.innerHTML = 'Mensaje enviado';
    confirmation.style.display = 'block';
    form.reset(); // limpiar el formulario
});

$(document).ready(function() {
  $("#search-button").click(function() {
    var searchText = $("#search-input").val().toLowerCase();
    $(".searchable").each(function() {
      var text = $(this).text().toLowerCase();
      if (text.indexOf(searchText) !== -1) {
        $(this).show();
      } else {
        $(this).hide();
      }
    });
  });
});
